# git-secrets

This is a test git repository to test secrets detection functionality in git. The primary goal is to prevent:

1. Committing secrets.
2. Merging commits with secrets on GitLab.
3. Prevent builds if there are secrets detected in the src code.

All of the above items work at different stages of a CI process but one thing which is common among all is detecting secrets in the source code, actually not just the recent commit but all the previous commits too. Note, this is not a vulnerability scanning.

There are multiple opensource tools available for detecting secrets present in your source code and version history. One of the most popular one is `gitleaks` which I've used in this sample repository.

## a. Committing Secrets

It is possible to prevent users from committing a code containing secrets using a feature of git itself, called precommit hooks. Precommit hooks provide a powerfull mechanism to run custom scripts/tools before git commits a change. If a precommit hook is failed the commit is cancel. This feature can be used to run a secret scanning tools against the code before commiting it to the local repo.

1. In order to use precommit hooks, you need to install pre-commit tool on your local machine. You can use `python-pip` or `conda` or a package manager such as `brew` to install it on your machine. You can follow the offical document to install it https://pre-commit.com/#install.

    Note: If you are using `pip`, then you might face issues on Windows such as commond not found even after properly configuring the path environment variables. In that case you can install it in a python virtual env and activate whenever you do git operations.

    Important: You'll also need `golang` installed on your machine which can follow from the official docs https://go.dev/doc/install.

2. In your existing or new git repository create a `.pre-commit-config.yaml` in the root. You can use the one (.pre-commit-config.yaml.sample) provided in this repo.

3. Then install the pre-commit hook in your repository (need to do it once or on first clone from remote).
    ```bash
    pre-commit install
    ```

4. Now `gitleaks` hook is ready and you can issue normal `git commit` (after staging changes) commands and check if it works as expected. 

## b. Merging Commits with Secrets on GitLab

There is no direct way to prevent gitlab from accepting commits pushed with secrets inside. You need to use server-side hooks but that you have access to only in self-hosted environments. Instead what we can do is prevent merging commits with secrets to the protected branches, using `gitlab-ci`. `gitlab-ci` allows you to write workflows which are triggered when specific events happen in your gitlab project, such as push, merge request creation, etc.

1. First you need to enable `Pipelines must succeed` and `Status checks must succeed` option in the `Settings -> Merge requests` for the gitlab project. In the same settings menu, enable `squash and merge strategy`. What this does is, once the user have removed the sensitive information, pushed the commit, and if the merge request pipeline passes, then before merging to the protected branch, all the commit history, specific to the merge request, is squashed and then merged to the protected branch. This way protected branch is always void of secrets in its commit history.  

2. Add branch protection rules so users can't directly push commits to the protected branches.

3. Create a CI workflow in `.gitlab-ci.yml` file in the repo root. You can use the one provided in the repo. It only scans the current commit for secrets.

4. Now you can commit and push unprotected branches to your gitlab project. But you can merge it to protected branches only if there are no secrets, detectable by `gitleaks` in the last commit. Also the protected branch will be void of commit histories with secrets.

## c. Prevent Builds if there are Secrets in the Source Code

This scenerio is applicable when you use are building artifacts from your source code in your CI pipeline. Even though we made sure that the protected branches don't have secrets but not all the branches will be protected. Ideally, only the production branch is protected while stage/prototype/dev branches aren't protect to reduce headache caused due to too many rules. In such cases we can only add workflow rules to make sure that the source code from which we are building our artifacts does not contain any sensitive information.

1. Use workflow to scan the code (in last commit) before continuing with the build workflow. You can take reference of my `gitlab-ci.yml`.

There are other numerous methods to prevent secrets from leaking into your CI/CD workflow. Above 3 methods can be useful and convenient to setup and cause little headache for the developers in terms of source code management.